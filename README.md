# [PlayDeca](https://playdeca.com)

This is the PlayDeca Repo for development of the site using **Bulma a modern CSS framework** based on [Flexbox](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Using_CSS_flexible_boxes).

<a href="https://playdeca.com"><img src="https://playdeca.com/assets/images/logos/PlayDecaBannerPNG.png" style="max-width:100%;" width="600"></a>

## Quick install

### NPM

```sh
npm install bulma
```

### Import
After installation, you can import the CSS file into your project using this snippet:

```sh
import 'bulma/css/bulma.css'
```

### CDN

[https://www.jsdelivr.com/package/npm/bulma](https://www.jsdelivr.com/package/npm/bulma)


## Browser Support

Bulma uses [autoprefixer](https://github.com/postcss/autoprefixer) to make (most) Flexbox features compatible with earlier browser versions. According to [Can I use](https://caniuse.com/#feat=flexbox), Bulma is compatible with **recent** versions of:

* Chrome
* Edge
* Firefox
* Opera
* Safari

Internet Explorer (10+) is only partially supported.

## Documentation

Browse the [online documentation here.](https://bulma.io/documentation/overview/start/)
